export enum Distance {
    BRATISLAVA_BUDAPEST = 200,
    BRATISLAVA_PRAGUE = 400,
    BUDAPEST_PRAGUE = 600,
}
