export enum CarGear {
    REVERSE = -1,
    NEUTRAL,
    DRIVE_1,
    DRIVE_2,
    DRIVE_3,
    DRIVE_4,
    DRIVE_5,
}
